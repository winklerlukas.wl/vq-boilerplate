import { Screen, Notify } from 'quasar'

export default () => {
  // set breakpoints
  Screen.setSizes({ xs: 768 })

  // notify default
  Notify.setDefaults({
    position: 'top-right',
    timeout: 2500,
    textColor: 'white',
    actions: [{ icon: 'close', color: 'white' }],
  })
}

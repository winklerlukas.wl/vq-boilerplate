import cloneDeep from 'lodash/cloneDeep'
import { isDeepEqual } from 'src/utils/helpers'

const ERROR_MESSAGES = {
  required: 'Toto pole je povinné',
  email: 'Email není ve správném formátu',
  zip: 'PSČ není ve správném formátu',
  ico: 'IČO není ve správném formátu',
  phone: 'Číslo není ve správném formátu',
  acceptTerms: 'Musíte vyjádřit souhlas s podmínkami',
  url: 'Adresa není validní',
  date: 'Datum není platné',
  dateBeforeNow: 'Datum musí být v minulosti',
  dateAfterNow: 'Datum musí být v budoucnosti.',
  minLength8: 'Minimální délka je 8 znaků.',
  sameAsNewPassword: 'Heslo se neshoduje.',
  maxValueLessThanPrice:
    'Požadovaná cena musí být menší než aktuální cena produktu.',
}

export default {
  props: {
    initialFormData: {
      type: Object,
      required: false,
    },
  },
  data: () => ({
    apiErrors: {},
    form: {
      fields: {},
      initial: {},
      pristine: true,
    },
  }),
  computed: {
    fieldHelpers() {
      return Object.keys(this.form.fields).reduce((acc, key) => {
        const errorMessage = this.getErrorMessage(key)
        if (this.$v) {
          const isDirty = this.$v.form.fields[key]
            ? this.$v.form.fields[key].$dirty
            : false
          acc[key] = {
            $attrs: {
              errorMessage,
              error: !!(isDirty && errorMessage),
              name: key,
            },
            $listeners: {
              blur: () => this.touch(key),
            },
          }
        } else {
          acc[key] = {
            $attrs: { name: key },
            $listeners: {},
          }
        }
        return acc
      }, {})
    },
  },
  methods: {
    // FORM HANDLERS
    handleSubmit() {
      if (this.$v) {
        this.touch()
        if (!this.$v.$error || this.$v.invalid) {
          this.submit()
        } else {
          this.$q.notify({
            message: 'Akci nelze provést. Zkontrolujte všechna pole.',
            color: 'secondary',
          })
        }
      } else {
        this.submit()
      }
    },
    resetForm() {
      this.form.fields = cloneDeep(this.form.initial)
      this.form.pristine = true
    },
    setForm(fields) {
      this.form.fields = cloneDeep(fields)
      this.form.initial = cloneDeep(fields)
    },
    initForm() {
      const initialFormData = Object.entries(this.model).reduce((acc, f) => {
        const [key, modelValue] = f
        const valueToSet =
          this.initialFormData &&
          this.initialFormData[key] !== undefined &&
          this.initialFormData[key] !== null
            ? this.initialFormData[key]
            : modelValue
        acc[key] = valueToSet
        return acc
      }, {})
      this.setForm(initialFormData)
    },
    // ERROR HANDLERS
    touch(field) {
      if (!this.$v) return
      if (field) {
        this.$v.form.fields[field] && this.$v.form.fields[field].$touch()
      } else {
        this.$v.$touch()
      }
    },
    getErrorMessage(field) {
      // Handle API errors
      if (this.apiErrors[field]) return this.apiErrors[field]

      // Handle Vuelidate errors
      if (!this.$v) return ''
      const vueliObj = this.$v.form.fields[field]

      if (!vueliObj || !vueliObj.$error) return ''

      const failedValidations = Object.keys(vueliObj).filter(
        key => !key.startsWith('$') && vueliObj[key] === false
      )
      const firstFailedValidation = failedValidations[0]

      return ERROR_MESSAGES[firstFailedValidation]
    },
    setApiErrors(response) {
      const responseData = response.data || response

      this.apiErrors = responseData
        .map(fieldError => ({
          field: fieldError.propertyPath,
          message: fieldError.message,
        }))
        .reduce((acc, curr) => {
          acc[curr.field] = curr.message
          return acc
        }, {})
    },
  },
  watch: {
    'form.fields': {
      handler(current) {
        this.form.pristine = isDeepEqual(current, this.form.initial)
      },
      deep: true,
    },
    initialFormData() {
      this.initForm()
    },
  },
  created() {
    this.initForm()
  },
}

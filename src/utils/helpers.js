import { isEqual, uniq } from 'lodash'

export const extractErrorObject = error => {
  console.log('errror: ', error.response)
  let message = ''
  let status = ''
  if (error.response) {
    message =
      error.response &&
      error.response.data &&
      (error.response.data.message ||
        error.response.data.error ||
        error.response.data)
    status = error.response.status
  } else {
    message = error.message || 'Error Happened'
    status = 'Fail'
  }
  return {
    message,
    status,
  }
}

export const isValidSlug = slug => {
  return /^[a-z0-9]+(?:-[a-z0-9]+)*$/.test(slug) || 'Not valid format'
}

export const isDeepEqual = (objectA, objectB) => {
  const keys = uniq(Object.keys(objectA).concat(Object.keys(objectB)))
  const notEqualKeys = []
  keys.forEach(key => {
    if (
      objectA[key] === undefined ||
      objectB[key] === undefined ||
      !isEqual(objectA[key], objectB[key])
    ) {
      notEqualKeys.push(key)
    }
  })
  const equal = !notEqualKeys.length
  return equal
}

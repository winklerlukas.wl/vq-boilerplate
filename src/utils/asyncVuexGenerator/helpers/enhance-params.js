import { normalizeUrl, parseEndpoint } from './otherUtils'

// enhance passed forms (form, id, etc == payload) with endpoint, apiInstance name, settings...
export default function enhanceParams(
  params = {},
  {
    fragment: { name, clear, endpoint = '', method, treatAsUpdate = false },
    settings: { apiGetter },
  }
) {
  let url = endpoint
  if (!params.url) {
    if (params.endpointParams && typeof params.endpointParams === 'object') {
      url = parseEndpoint(url, params.endpointParams)
    }
    url = normalizeUrl(`/${url}${params.query || ''}`)
  }
  const request = {
    method: method.toLowerCase(),
    url: params.url || url,
    params: params.params || {},
  }
  if (params.data) {
    request.data = params.data
  }
  return {
    request,
    actionSettings: {
      method,
      treatAsUpdate,
      name,
      apiGetter,
      clear: !!(params.clear || clear),
      dataPath: params.dataPath || undefined,
      dataTransformer: params.dataTransformer || undefined,
    },
  }
}

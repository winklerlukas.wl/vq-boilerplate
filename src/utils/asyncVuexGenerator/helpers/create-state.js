// initial state in store for action
const stateTypes = {
  GET: {
    data: null,
    error: null,
    loading: false,
  },
  PATCH: {
    data: null,
    updateError: null,
    updateLoading: false,
  },
  POST: {
    data: null,
    error: null,
    loading: false,
  },
  DELETE: {
    data: null,
    error: null,
    loading: false,
  },
}

const createState = ({ method, treatAsUpdate }) => {
  const type = treatAsUpdate ? 'PATCH' : method || 'GET'
  return stateTypes[type]
}

export default createState

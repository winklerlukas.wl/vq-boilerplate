import camelCase from 'lodash/camelCase'

import createAction from './create-action'
import createMutations from './create-mutations'
import createState from './create-state'
import enhanceParams from './enhance-params'

export default function generateStore({ fragments, state = {}, settings }) {
  // inital module store object
  const store = {
    mutations: {},
    actions: {},
    state,
  }
  // cycle through all fragments and generate actions, mutations, state
  fragments.forEach(fragment => {
    const generatedStoreFragment = generateStoreFragment(fragment)
    // parse actionName
    const actionName = camelCase(
      `${fragment.name}${resolveSuffix(fragment.suffix)}`
    )
    // enhance params passed to action (eg. form, id,...) with endpoint, url, etc.
    // and add action to actions
    store.actions[actionName] = (context, params) => {
      const enhancedParams = enhanceParams(params, { fragment, settings })
      return generatedStoreFragment.action(context, enhancedParams)
    }
    // add generated mutations
    store.mutations = {
      ...store.mutations,
      ...generatedStoreFragment.mutations,
    }

    // merge inital state and fragments' state and add that all to module state
    // if this part is already added
    if (!fragment.stateless && fragment.stateKey) {
      if (store.state[fragment.stateKey]) {
        store.state[fragment.stateKey] = {
          ...store.state[fragment.stateKey],
          ...generatedStoreFragment.state,
        }
        // if this part is not already added
      } else {
        store.state[fragment.stateKey] = generatedStoreFragment.state
      }
    }
  })
  return store
}

function typeOf(obj) {
  return {}.toString
    .call(obj)
    .split(' ')[1]
    .slice(0, -1)
    .toLowerCase()
}

function generateStoreFragment({
  name,
  method,
  stateKey,
  stateless,
  clear,
  treatAsUpdate = false,
}) {
  const STATELESS = stateless || !stateKey
  // if fragment has no "stateKey" or it is "stateless", mutations and state will not be created
  const action = createAction({ method, stateless: STATELESS })
  const mutations = STATELESS
    ? {}
    : createMutations({ name, stateKey, method, clear, treatAsUpdate })
  const state = STATELESS ? {} : createState({ method, treatAsUpdate })
  return { mutations, action, state }
}

function resolveSuffix(suffix) {
  const suffixType = typeOf(suffix)
  if (suffix) {
    return suffixType === 'string' ? `_${suffix}` : '_GENERATED'
  } else {
    return ''
  }
}

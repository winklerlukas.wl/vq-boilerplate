const createMutationTypes = ({ name, stateKey }) => ({
  stateKey: stateKey,
  // standard
  PENDING: `${name}_PENDING`,
  PENDING_CLEAR: `${name}_PENDING_CLEAR`,
  SUCCESS: `${name}_SUCCESS`,
  SUCCESS_CLEAR: `${name}_SUCCESS_CLEAR`,
  FAILURE: `${name}_FAILURE`,
  FAILURE_CLEAR: `${name}_FAILURE_CLEAR`,
  // update
  UPDATE_PENDING: `${name}_UPDATE_PENDING`,
  UPDATE_PENDING_CLEAR: `${name}_UPDATE_PENDING_CLEAR`,
  UPDATE_SUCCESS: `${name}_UPDATE_SUCCESS`,
  UPDATE_SUCCESS_CLEAR: `${name}_UPDATE_SUCCESS_CLEAR`,
  UPDATE_FAILURE: `${name}_UPDATE_FAILURE`,
  UPDATE_FAILURE_CLEAR: `${name}_UPDATE_FAILURE_CLEAR`,
  // clear state
  CLEAR_STATE: `${name}_CLEAR_STATE`,
})

const createMutationsFunctions = ({
  mutationTypes,
  method,
  treatAsUpdate,
  clear,
}) => {
  // standard
  // CLEAR state
  if (method === 'CLEAR') {
    return {
      [mutationTypes.CLEAR_STATE](state) {
        const clearState = {
          data: null,
          loading: false,
          error: null,
        }
        if (
          state[mutationTypes.stateKey].updateLoading !== undefined ||
          state[mutationTypes.stateKey].updateError !== undefined
        ) {
          clearState.updateError = null
          clearState.updateLoading = false
        }
        state[mutationTypes.stateKey] = {
          ...state[mutationTypes.stateKey],
          ...clearState,
        }
      },
    }
  }
  // UPDATE
  if (method === 'PATCH' || treatAsUpdate) {
    // UPDATE CLEAR
    if (clear) {
      return {
        [mutationTypes.UPDATE_PENDING_CLEAR](state) {
          state[mutationTypes.stateKey] = {
            ...state[mutationTypes.stateKey],
            error: null,
            updateLoading: true,
            data: null,
            updateError: null,
          }
        },
        [mutationTypes.UPDATE_SUCCESS_CLEAR](state, payload) {
          state[mutationTypes.stateKey] = {
            ...state[mutationTypes.stateKey],
            updateLoading: false,
            data: payload,
            updateError: null,
          }
        },
        [mutationTypes.UPDATE_FAILURE_CLEAR](state, payload) {
          state[mutationTypes.stateKey] = {
            ...state[mutationTypes.stateKey],
            updateLoading: false,
            data: null,
            updateError: payload,
          }
        },
      }
    }
    // UPDATE NORMAL
    return {
      [mutationTypes.UPDATE_PENDING](state) {
        state[mutationTypes.stateKey] = {
          ...state[mutationTypes.stateKey],
          error: null,
          updateLoading: true,
        }
      },
      [mutationTypes.UPDATE_SUCCESS](state, payload) {
        state[mutationTypes.stateKey] = {
          ...state[mutationTypes.stateKey],
          updateLoading: false,
          data: payload,
          updateError: null,
        }
      },
      [mutationTypes.UPDATE_FAILURE](state, payload) {
        state[mutationTypes.stateKey] = {
          ...state[mutationTypes.stateKey],
          updateLoading: false,
          updateError: payload,
        }
      },
    }
  }
  // REGULAR
  return {
    [mutationTypes.PENDING](state) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: true,
      }
    },
    [mutationTypes.PENDING_CLEAR](state) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: true,
        data: null,
        error: null,
      }
    },
    [mutationTypes.SUCCESS](state, payload) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: false,
        data: payload,
        error: null,
      }
    },
    [mutationTypes.SUCCESS_CLEAR](state, payload) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: false,
        data: payload,
        error: null,
      }
    },
    [mutationTypes.FAILURE](state, payload) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: false,
        error: payload,
      }
    },
    [mutationTypes.FAILURE_CLEAR](state, payload) {
      state[mutationTypes.stateKey] = {
        ...state[mutationTypes.stateKey],
        loading: false,
        data: null,
        error: payload,
      }
    },
  }
}

const createMutations = ({
  name,
  stateKey,
  method,
  clear,
  treatAsUpdate = false,
}) => {
  const mutationTypes = createMutationTypes({ name, stateKey })
  return createMutationsFunctions({
    mutationTypes,
    method,
    clear,
    treatAsUpdate,
  })
}

export default createMutations

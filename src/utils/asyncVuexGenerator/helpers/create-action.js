import clearState from '../actions/clear-state'
import mainAction from '../actions/main-action'
import mainActionStateless from '../actions/main-action-stateless'

const createAction = ({ method, stateless = false }) => {
  if (method === 'CLEAR') {
    return clearState
  } else {
    return stateless ? mainActionStateless : mainAction
  }
}

export default createAction

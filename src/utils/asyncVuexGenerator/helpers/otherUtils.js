export function getDeepValue(obj, path) {
  return path.split('.').reduce((acc, curr) => {
    return acc[curr]
  }, obj)
}

export function normalizeUrl(url) {
  return url.replace(/(\/+)/gi, '/').replace(/(\/$)/gi, '')
}

export function parseEndpoint(endpoint, params) {
  let url = endpoint
  Object.entries(params).forEach(([key, value]) => {
    url = url.replace(`:${key}`, value)
  })
  return url
}

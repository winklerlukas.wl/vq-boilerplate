import { getDeepValue } from '../helpers/otherUtils'

const mutationNameResolver = parts => {
  return parts.filter(p => !!p).join('_')
}

export default async (
  { commit },
  {
    actionSettings: {
      name,
      clear,
      apiGetter,
      method,
      treatAsUpdate,
      dataPath,
      dataTransformer,
    },
    request,
  }
) => {
  // apiInstance
  const API = apiGetter()

  // resolve mutation names
  const UPDATE = method === 'PATCH' || treatAsUpdate ? 'UPDATE' : ''
  const CLEAR = clear ? 'CLEAR' : ''

  const mutationNamePending = mutationNameResolver([
    name,
    UPDATE,
    'PENDING',
    CLEAR,
  ])
  const mutationNameSuccess = mutationNameResolver([
    name,
    UPDATE,
    'SUCCESS',
    CLEAR,
  ])
  const mutationNameFailure = mutationNameResolver([
    name,
    UPDATE,
    'FAILURE',
    CLEAR,
  ])

  // pending
  commit(mutationNamePending)

  // http async request
  try {
    // fetch data
    const response = await API(request)
    // set data and stop loading
    let data = response.data
    if (dataPath) {
      data = getDeepValue(data, dataPath)
    }
    if (dataTransformer) {
      data = dataTransformer(data)
    }
    commit(mutationNameSuccess, data)
    return response
  } catch (error) {
    // set error and stop loading
    commit(mutationNameFailure, error)
    throw error
  }
}

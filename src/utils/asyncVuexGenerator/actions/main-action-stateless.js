export default async (_, { actionSettings: { apiGetter }, request }) => {
  // apiInstance
  const apiInstance = apiGetter()

  // http async request
  try {
    // fetch data
    return await apiInstance(request)
  } catch (error) {
    throw error
  }
}

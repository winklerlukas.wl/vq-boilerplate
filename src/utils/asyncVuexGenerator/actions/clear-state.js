export default ({ commit }, { actionSettings: { name } }) => {
  // resolve mutation name
  const mutationName = `${name}_CLEAR_STATE`
  // commit
  commit(mutationName)
}

import axios from 'axios'

import generateStore from './helpers/generate-store'

/* eslint-disable no-unused-vars */
// Ice factory pattern
export default function VuexFactory(config = {}) {
  let state = config.state || {}
  /*
  state example:
    {
      detail: {
        myCustomProp: 'Random string'
      }
    }
    // if any of your fragments has already stateKey "detail", it will be merged with this object:
    // eg. final state:
    // {
    //   detail: {
    //     data: null,
    //     error: null,
    //     loading: false,
    //     myCustomProp: 'Random string'
    //   }
    // }
  */
  let fragments = config.fragments || []
  /*
    fragments example:
    fragments = [
      {
        name: 'GET_LIST',               // action/mutation name => action = getList; mutation == GET_LIST
        stateKey: 'list',               // path in store eg. state.module.list - if not set mutations and state for this fragment will not be created
        endpoint: 'pokemons/:id',       // your api endpoint - if action gets endpointParams (object), endpoint params will be replaced by values in object. eg. endpointParams = { id: '1221213' }
        method: 'GET',                    // HTTP method supports ['GET', 'POST', 'DELETE', 'PATCH'] + custom 'CLEAR' => reset state.module.list
        suffix: true,                   // [String, Boolean] - action name modifier => if true action = getListGenerated; if string (eg. 'factored') action = getListFactored
        clear: true,                    // optional - if true, "data" and "error" in state (module.[statekey]) will be set to null on pending
        treatAsUpdate: true             // mutations will be same as for "PATCH" method (with "_UPDATE" suffix)
      },
    ]
  */
  let settings = config.settings
  if (!settings.apiGetter) {
    settings.apiGetter = () => axios.create()
  }
  /*
  settings example:
    {
      apiGetter: YourApiGetter
        // YourApiGetter is imported function which returns your axios instance -
        // it is called by every generated action during the process
        // if not set, returns default blank axios instance
    }
   // if not set generator will use default axios
  */
  return Object.freeze({
    getStore,
    setState,
    setFragments,
    state,
    setSettings,
  })

  function setFragments(payload) {
    fragments = payload
  }

  function setState(payload) {
    state = payload
  }

  function setSettings(payload) {
    settings = payload
  }

  function getStore() {
    const storeConfig = {
      state,
      fragments,
      settings,
    }
    return generateStore(storeConfig)
  }
  // returns { mutations, state, actions }
}

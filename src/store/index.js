import Vue from 'vue'
import Vuex from 'vuex'
import * as modules from './modules'

Vue.use(Vuex)

const Store = new Vuex.Store({
  modules,
  actions: {
    // not moduled actions
  },
  mutations: {
    // not moduled mutations
  },
  state: () => ({
    // not moduled state
  }),

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV,
})
export default Store

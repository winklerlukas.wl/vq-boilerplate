import generated from './generated'

export default {
  /* GENERATED */
  ...generated.mutations,

  /* CUSTOM */
  SET_PLANETS_PAGINATION(state, payload) {
    state.planetsList.pagination = {
      ...state.planetsList.pagination,
      ...payload,
    }
  },
}

import { getApiInstance } from 'src/api/SWAPI'
import AsyncVuexGenerator from 'src/utils/AsyncVuexGenerator'

const defaultState = {
  planetsList: {
    pagination: {
      sortBy: 'name',
      descending: false,
      page: 1,
      rowsPerPage: 25,
      rowsNumber: 0,
    },
  },
}
const fragments = [
  // GET //
  {
    name: 'GET_PLANETS_LIST',
    stateKey: 'planetsList',
    endpoint: 'planets',
    method: 'GET',
    suffix: true,
  },
  {
    name: 'GET_PLANET_DETAIL',
    stateKey: 'planetDetail',
    // note - this is same as in LIST fragment, you have to handle '/:id'
    // manually with custom action using a generated one
    endpoint: 'planets/:id',
    method: 'GET',
    suffix: true,
    clear: true,
  },
]

const generatedStore = AsyncVuexGenerator({
  settings: {
    apiGetter: getApiInstance,
  },
  fragments,
  state: defaultState,
}).getStore()

export default generatedStore
export const { actions, state, mutations } = generatedStore

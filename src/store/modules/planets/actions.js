import { actions } from './generated'

export default {
  /* GENERATED */
  ...actions,

  /* CUSTOM */

  // this action uses generated one, so you dont need to create or commit any mutations,
  // mutations are handled by the generated action
  // you only "aggregate" the generated action by specific params - url with query params ("fetchQuery")
  async getPlanetsList({ dispatch, state, commit }) {
    // build query with helper from pagination stored in vuex
    const { page, rowsPerPage } = state.planetsList.pagination
    try {
      const response = await dispatch('getPlanetsListGenerated', {
        params: {
          limit: rowsPerPage,
          offset: (page - 1) * rowsPerPage,
        },
      })
      commit('SET_PLANETS_PAGINATION', { rowsNumber: response.data.count })
    } catch (e) {
      throw e
    }
  },

  // this action uses generated one, so you dont need to create or commit any mutations,
  // mutations are handled by the generated action
  // you only "aggregate" the generated action by specific params - url with query params ("fetchQuery")
  async getPlanetDetail({ dispatch }, { id }) {
    try {
      await dispatch('getPlanetDetailGenerated', {
        endpointParams: {
          id,
        },
      })
    } catch (e) {
      throw e
    }
  },
}

import auth from './auth'
import pokemons from './pokemons'
import planets from './planets'

export { auth, pokemons, planets }

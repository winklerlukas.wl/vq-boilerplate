import { getApiInstance } from 'src/api/PokemonAPI'
import AsyncVuexGenerator from 'src/utils/AsyncVuexGenerator'

const defaultState = {
  pokemonsList: {
    pagination: {
      sortBy: 'name',
      descending: false,
      page: 1,
      rowsPerPage: 25,
      rowsNumber: 0,
    },
  },
}
const fragments = [
  // GET //
  {
    name: 'GET_POKEMONS_LIST',
    stateKey: 'pokemonsList',
    endpoint: 'pokemon',
    method: 'GET',
    suffix: true,
  },
  {
    name: 'GET_POKEMON_COLOR_LIST',
    stateKey: 'pokemonsColorList',
    endpoint: 'pokemon-color',
    method: 'GET',
    suffix: true,
  },
  {
    name: 'GET_POKEMON_DETAIL',
    stateKey: 'pokemonDetail',
    // note - this is same as in LIST frafment, you have to handle '/:name'
    // manually with custom action using a generated one
    endpoint: 'pokemon/:name',
    method: 'GET',
    suffix: true,
    clear: true,
  },
]

const generatedStore = AsyncVuexGenerator({
  settings: {
    apiGetter: getApiInstance,
  },
  fragments,
  state: defaultState,
}).getStore()

export default generatedStore
export const { actions, state, mutations } = generatedStore

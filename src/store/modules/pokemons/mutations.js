import generated from './generated'

export default {
  /* GENERATED */
  ...generated.mutations,

  /* CUSTOM */
  SET_POKEMONS_PAGINATION(state, payload) {
    state.pokemonsList.pagination = {
      ...state.pokemonsList.pagination,
      ...payload,
    }
  },
}

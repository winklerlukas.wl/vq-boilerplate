import generated from './generated'

export default {
  /* GENERATED */
  ...generated.actions,

  /* CUSTOM */

  // this action uses generated one, so you dont need to create or commit any mutations,
  // mutations are handled by the generated action
  // you only "aggregate" the generated action by specific params - url with query params ("fetchQuery")
  async getPokemonsList({ dispatch, state, commit }) {
    const { page, rowsPerPage } = state.pokemonsList.pagination
    try {
      const response = await dispatch('getPokemonsListGenerated', {
        params: {
          limit: rowsPerPage,
          offset: (page - 1) * rowsPerPage,
        },
      })
      commit('SET_POKEMONS_PAGINATION', { rowsNumber: response.data.count })
    } catch (e) {
      throw e
    }
  },

  // this action uses generated one, so you dont need to create or commit any mutations,
  // mutations are handled by the generated action
  // you only "aggregate" the generated action by specific params - url with query params ("fetchQuery")
  async getPokemonDetail({ dispatch }, { name }) {
    try {
      await dispatch('getPokemonDetailGenerated', {
        endpointParams: {
          name,
        },
      })
    } catch (e) {
      throw e
    }
  },
}

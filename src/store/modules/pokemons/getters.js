export default {
  pokemonsPagination: state => {
    return state.pokemonsList.pagination
  },
}

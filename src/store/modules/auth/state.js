import { Cookies } from 'quasar'

function getUserOnInit() {
  const user = Cookies.get('user')
  return {
    user: user || null,
  }
}

export default () => ({
  loading: false,
  user: null,
  error: null,
  ...getUserOnInit(),
})

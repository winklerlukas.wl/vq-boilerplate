export default {
  isAuthorized: state => {
    return !!state.user
  },
  userToken: state => {
    return state.user && state.user.token ? state.user.token : ''
  },
}

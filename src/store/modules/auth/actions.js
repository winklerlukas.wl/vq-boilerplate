// import api from 'src/api'
import { Cookies } from 'quasar'
import Router from 'src/router'

function setCookiesAndLocalStorage({ rememberMe, user }) {
  // this could be localStorage if app is only SPA
  const cookieOptions = rememberMe ? { expires: 10 } : {}
  Cookies.set('user', user, cookieOptions)
  localStorage.setItem('rememberMe', rememberMe)
  // here you can set user token to ApiInstance eg.:
  // api['PokemonAPI'].defaults.headers.common['Authorization'] = user.token
  // TODO set user token; rename function
}

function clearCookies() {
  // this could be localStorage if app is only SPA
  Cookies.remove('user')
  // here you can remove user token from ApiInstance
  // TODO remove user token; rename function
}

export default {
  setUser({ commit }, user) {
    commit('LOGIN_SUCCESS', user)
  },
  async login(
    { commit, dispatch },
    { email = '', password = '', rememberMe = false }
  ) {
    commit('LOGIN_PENDING')
    let response
    try {
      // response = await api['your-auth-api].post('your_login_api_url', { email, password })
      // fake request
      response = await new Promise(resolve => {
        const fakeUser = {
          firstname: 'Šákul',
          surname: 'Relkniw',
          id: '999-000-999',
          email,
          password,
          token: 'token',
        }
        setTimeout(() => resolve({ data: fakeUser }), 300)
      })
      setCookiesAndLocalStorage({ rememberMe, user: response.data })
      dispatch('setUser', response.data)
      Router.push('/')
    } catch (e) {
      commit('LOGIN_FAILED', e)
      throw new Error(e)
    }
    return response
  },
  logout({ commit }) {
    commit('LOGOUT')
    clearCookies()
    Router.push('/login')
  },
  async register({ dispatch }, payload) {
    let response
    try {
      // response = await api['your-auth-api].post('your_register_api_url', payload)
      // fake request
      response = await new Promise(resolve => {
        const fakeUser = {
          firstname: 'Šákul',
          surname: 'Relkniw',
          id: '999-000-999',
          payload,
          token: 'token',
        }
        setTimeout(() => resolve(fakeUser), 300)
      })
      setCookiesAndLocalStorage({ rememberMe: false, user: response.data })
      dispatch('setUser', response.data)
    } catch (e) {
      throw e.response.data
    }
    return response
  },
}

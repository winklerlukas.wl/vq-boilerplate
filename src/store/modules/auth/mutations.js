export default {
  LOGIN_PENDING(state) {
    state.loading = true
  },
  LOGIN_SUCCESS(state, data) {
    state.loading = false
    state.error = null
    state.user = data
  },
  LOGIN_FAILED(state, error) {
    state.loading = false
    state.error = error
    state.user = null
  },
  LOGOUT(state) {
    state.loading = false
    state.error = null
    state.user = null
  },
}

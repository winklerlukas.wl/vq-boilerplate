import axios from 'axios'

const SWAPI = axios.create({
  baseURL: 'https://swapi.co/api/',
})

// Default params
// PokemonAPI.defaults.params = {}
// PokemonAPI.defaults.params['your_param'] = your_param

// Default headers
// PokemonAPI.headers.common['your_header'] = your header

const getApiInstance = () => {
  return SWAPI
}

export { getApiInstance }

export default SWAPI

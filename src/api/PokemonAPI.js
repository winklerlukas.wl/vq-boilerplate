import axios from 'axios'

const PokemonAPI = axios.create({
  baseURL: 'https://pokeapi.co/api/v2/',
})

// Default params
// PokemonAPI.defaults.params = {}
// PokemonAPI.defaults.params['your_param'] = your_param

// Default headers
// PokemonAPI.headers.common['your_header'] = your header

const getApiInstance = () => {
  return PokemonAPI
}

export { getApiInstance }

export default PokemonAPI

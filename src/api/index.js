import axios from 'axios'
import PokemonAPI from './PokemonAPI'
import SWAPI from './SWAPI'

export default {
  PokemonAPI,
  SWAPI,
  axios,
}

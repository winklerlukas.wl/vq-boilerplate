import store from '../store'

const loadPage = page => () => import(`pages/${page}`)
const loadLayout = layout => () => import(`layouts/${layout}`)

const ifNotAuthenticated = (to, from, next) => {
  const isAuthenticated = store.getters['auth/isAuthenticated']
  if (!isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  const isAuthenticated = store.getters['auth/isAuthenticated']

  if (isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export { loadPage, ifNotAuthenticated, ifAuthenticated, loadLayout }

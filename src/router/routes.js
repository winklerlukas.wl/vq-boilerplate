import { loadLayout, loadPage } from './helpers'

const routes = [
  {
    path: '/login',
    name: 'LoginPage',
    component: loadPage('LoginPage'),
  },
  {
    path: '/',
    component: loadLayout('MainLayout'),
    children: [
      {
        path: '',
        component: loadPage('Index'),
      },
      {
        path: 'pokemons',
        component: loadPage('PokemonsPage'),
      },
      {
        path: 'pokemons/:name',
        component: loadPage('PokemonPage'),
      },
      {
        path: 'planets',
        component: loadPage('PlanetsPage'),
      },
      {
        path: 'planets/:id',
        component: loadPage('PlanetPage'),
      },
    ],
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: loadPage('Error404'),
  })
}

export default routes
